import pandas as pd
import random
import matplotlib.pyplot as plt
import numpy as np
from random import *

addData = pd.DataFrame();

prodCodeList = ["S001","S002","S003","S004","S005","S006","S007","S008","S009","S010","S011","S012","S013","S014","S015","S016","S017","S018","S019","S020","S021","S022","S023","S024","S025","S026"]

"""
적금 상품 가입 정보 생성
"""
def generateHistory(hCount, iCount):
    ageSeries = pd.Series()
    creSeries = pd.Series()
    fundSeries = pd.Series()
    loanSeries = pd.Series()
    prodSeries = pd.Series()
    annualSeries = pd.Series()

    #
    # 고객 나이
    #
    ageList = np.random.binomial(40, 0.3, hCount)

    count = [1, 2, 5, 10, 25, 40, 37, 45, 63, 66,
             80, 95, 92, 130, 150, 104, 64, 32, 20, 12, 15,
             8, 9, 9, 10, 10, 8, 6, 3, 2, 2, 5]

    for age in range(20, 50):
        iCount = (count[int(age/20)] * 99) + randrange(500, 1000)

        ageSeries = ageSeries.append(pd.Series([age for i in range(0, iCount)]))

        #
        # 신용등급
        #
        creLevel = np.random.binomial(5, 0.5, iCount)
        creSeries = creSeries.append(pd.Series(creLevel))

        #
        # 펀드 개수
        #
        fundList = np.random.binomial(5, 0.3, iCount)
        fundSeries = fundSeries.append(pd.Series(fundList))

        #
        # 대출상품 개수
        #
        loanList = np.random.binomial(3, 0.3, iCount)
        loanSeries = loanSeries.append(pd.Series(loanList))

        #
        # 상품컬럼
        #
        prodList = [prodCodeList[randrange(26)] for i in range(0, iCount)]
        prodSeries = prodSeries.append(pd.Series(prodList))

        #
        # 연봉
        #
        annList = np.random.binomial(10, 0.6, iCount) # * 1000
        annualSeries = annualSeries.append(pd.Series(annList))


    return pd.DataFrame({
            'age': ageSeries,
            'creLevel': creSeries,
            'fundCount': fundSeries,
            'loanCount': loanSeries,
            'prodCode': prodSeries,
            'annual': annualSeries
        })


if __name__ == '__main__':
    hCnt = 100
    iCnt = 100

    s0 = generateHistory(hCnt, iCnt)
    s0['gender'] = 0

    s1 = generateHistory(hCnt, iCnt)
    s1['gender'] = 1

    allData = pd.concat([s0, s1])
    print(allData.head())
    print(allData.shape)

    allData.to_csv('save2.csv', sep=',')