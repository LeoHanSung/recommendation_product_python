FROM choies_ubuntu:latest
COPY recomServer.py /app
COPY knnModel.bin /app
WORKDIR /app
ENTRYPOINT ["python3"]
CMD ["recomServer.py"]