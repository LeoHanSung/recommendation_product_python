import json
import flask
from flask import request
import pickle
import numpy as np

#
# 상품
#
PROD_CODE_LIST = ["S001","S002","S003","S004","S005","S006","S007","S008","S009","S010","S011","S012","S013","S014","S015","S016","S017","S018","S019","S020","S021","S022","S023","S024","S025","S026"]

# 포트 번호
TM_PORT_NO = 8085

# HTTP 서버 실행하기
app = flask.Flask(__name__)
print("http://localhost:" + str(TM_PORT_NO))

"""
상품추천
"""
fp = open('knnModel.bin', 'rb')
knnModel = pickle.load(fp)
fp.close()

@app.route('/', methods=['GET'])
def index():
    return json.dumps({
        "status": "ALIVE !!"
    })


@app.route('/recommend', methods=['GET'])
def recommend():
    # URL 매개 변수 추출하기

    q = request.args.get('userAttr', '')
    if q == '':
        return json.dumps({
            "status": "내용을 입력해주세요"
        })

    inAttr = list(map(int, q.split(",")))

    # val = knnModel.predict_proba([[20, 1, 3, 2, 5, 0]])
    val = knnModel.predict_proba([inAttr])

    idx = np.where(val[0] > 0.0)[0]
    prIdx = list(idx)

    pCode = []
    for i in prIdx:
        pCode.append(PROD_CODE_LIST[i])

    # 결과를 JSON으로 출력하기
    return json.dumps({
        "prodList": pCode
    })


if __name__ == '__main__':
    # 서버 실행하기
    app.run(debug=False, port=TM_PORT_NO, host="0.0.0.0")