import pandas as pd
from sklearn.neighbors import KNeighborsClassifier # import kNN from scikit learn
from sklearn.model_selection import train_test_split
import sklearn.metrics as metrics
import pickle

# 데이터 읽기
prdData = pd.read_csv('save2.csv')

# 상품코드를 라벨로 사용한다.
labels = prdData['prodCode'].values

# 상품코드를 삭제한다.
prdData.drop('Unnamed: 0', axis=1, inplace=True)
prdData.drop('prodCode', axis=1, inplace=True)

# 상품에 가입한 사람의 속성정보
# age(나이), creLevel(신용등급), fundCount(가입 펀드수), loanCount(대출 상품수), annual(연봉), gender(성별)
datas = prdData.values

# 훈력, 테스트 데이터 분리
x_train, x_test, y_train, y_test = train_test_split(datas, labels, test_size=0.2)

# 모델 생성 및 학습
knnModel = KNeighborsClassifier(n_neighbors = 4) #, weights="distance") # define model
knnModel.fit(x_train, y_train)

y_pred = knnModel.predict(x_test)
acc = metrics.accuracy_score(y_test, y_pred)
rep = metrics.classification_report(y_test, y_pred)

print('*' * 80)
print('정답률 = ', acc)
print(rep)
print('*' * 80)

# 모델 저장
fp = open('knnModel.bin', 'wb')
pickle.dump(knnModel, fp)
fp.close()

print('모델 저장!!!')